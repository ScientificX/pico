import { ParserState } from "./types";

export const initState = (targetString: string): ParserState => {
  return {
    index: 0,
    targetString: targetString,
    result: [],
    error: null,
  };
};

