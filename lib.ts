import { ParserState, Result, ParserFn } from "./types";
import { initState } from "./initialStates";

class Parser {
  parserStateTransformerFn: ParserFn;
  constructor(parserStateTransformerFn: ParserFn) {
    this.parserStateTransformerFn = parserStateTransformerFn;
  }

  run(targetString: string) {
    const init = initState(targetString);

    return this.parserStateTransformerFn(init);
  }

  map(fn: Function) {
    return new Parser((parserState) => {
      const nextState = this.parserStateTransformerFn(
        parserState
      ) as ParserState;
      if (nextState.error) return nextState;

      return injectResult(fn(nextState.result), nextState);
    });
  }

  errorMap(fn: Function) {
    return new Parser((parserState) => {
      const nextState = this.parserStateTransformerFn(
        parserState
      ) as ParserState;
      if (nextState.error) {
        return injectError(nextState, fn(nextState.error));
      }
      return nextState;
    });
  }

  chain(fn: Function) {
    return new Parser((parserState: ParserState) => {
      let nextState = this.parserStateTransformerFn(parserState) as ParserState;

      if (nextState.error) return nextState;
      let nextParser = fn(nextState);

      return nextParser.parserStateTransformerFn(nextState);
    });
  }

  ap(p: Parser) {
    // Container(f).ap(Container(a)) => Container(f(a))
    return new Parser((a: ParserState) => {
      let b = p.parserStateTransformerFn(a) as ParserState;
      let c = this.parserStateTransformerFn(b) as Function;
      return c(b);
    });
  }

  // liftA (states : Parser[]) {
  // Generally parsers are of the form (Parser type)
  // At the time this function is invoked this parser must be of type Parser {\a1 a2 a3 -> val}
  // when called exprP.liftA([parser1, parser2, parser3, ...]) iter
  //
  // })
  //   return new Parser( (parserState : ParserState) => {
  //     let f = this.parserStateTransformerFn(parserState);
  //     states.reduce((acc, curr) => )
  //   });
  // }
}

const injectResult = (newResult: any, prevState: ParserState): ParserState => {
  return {
    ...prevState,
    // index: prevState.index + prevState.result.reduce(((acc:any, curr:any) => curr.length + acc), 0),
    result: newResult,
  };
};

const injectError = (prevState: ParserState, erorr: string): ParserState => {
  return {
    ...prevState,
    error: erorr,
  };
};

const str = (s: string) =>
  new Parser((prevState: ParserState): ParserState => {
    let { index, targetString } = prevState;

    if (targetString.slice(index).startsWith(s)) {
      let result: string = s;
      return injectResult(result, {
        ...prevState,
        index: prevState.index + s.length,
      });
    }
    return injectError(
      prevState,
      `couldnt match ${targetString.slice(index)} with ${s}`
    );
  });

const m_sequenceOf =
  (parsers: ParserFn[]) =>
  (prevState: ParserState): ParserState => {
    return parsers.reduce<ParserState>(
      (prev, curr) => curr(prev) as ParserState,
      prevState
    );
  };

const sequenceOf = (parsers: Parser[]) =>
  new Parser((prevState: ParserState): ParserState => {
    let output: string[] = [];
    let nextState = prevState;

    for (let parser of parsers) {
      nextState = parser.parserStateTransformerFn(nextState) as ParserState;
      if (nextState.error)
        return injectError(prevState, `parser in sequence failed at ${nextState.index}`);
      output.push(nextState.result);
    }
    return injectResult(output, nextState);
  });

const choice = (parsers: Parser[]) =>
  new Parser((prevState: ParserState): ParserState => {
    if (prevState.error) return prevState;

    for (let parser of parsers) {
      let nextState = parser.parserStateTransformerFn(prevState) as ParserState;
      if (!nextState.error) {
        return nextState;
      }
    }
    return injectError(prevState, `couldnt match with any choice`);
  });

const many = (parser: Parser) =>
  new Parser((parserState) => {
    let done = false;
    let output = [];
    let nextParserState = parserState;
    while (!done) {
      let intermediateState = parser.parserStateTransformerFn(
        nextParserState
      ) as ParserState;
      if (intermediateState.error == null) {
        output.push(intermediateState.result);
        nextParserState = intermediateState;
      } else {
        done = true;
      }
    }
    // console.log(output)
    return injectResult(output, nextParserState);
  });

const many1 = (parser: Parser) =>
  new Parser((parserState) => {
    let done = false;
    let output = [];
    let nextParserState = parserState;
    while (!done) {
      let intermediateState = parser.parserStateTransformerFn(
        nextParserState
      ) as ParserState;
      if (intermediateState.error == null) {
        output.push(intermediateState.result);
        nextParserState = intermediateState;
      } else {
        done = true;
      }
    }
    if (!(output.length >= 1)) {
      return injectError(parserState, `couldnt match at least one`);
    }
    return injectResult(output, nextParserState);
  });

//sepBy takes a sep parser and a value parser returns a list of values parser seperated by

const sepBy = (sep: Parser) => (value: Parser) => {
  return many(sequenceOf([value, sep, value]));
};

const digits = new Parser((parserState) => {
  let digitsRegx = /^[0-9]+/;
  let ans = parserState.targetString.slice(parserState.index).match(digitsRegx);
  if (ans && ans[0]) {
    return injectResult(ans[0], {
      ...parserState,
      index: parserState.index + ans[0].length,
    });
  }
  return injectError(
    parserState,
    `couldnt match any digits at ${parserState.index}`
  );
});

const letters = new Parser((parserState) => {
  let lettersRegx = /^[A-Za-z]+/;

  let ans = parserState.targetString
    .slice(parserState.index)
    .match(lettersRegx);
  if (ans && ans[0]) {
    return injectResult(ans[0], {
      ...parserState,
      index: parserState.index + ans[0].length,
    });
  }
  return injectError(
    parserState,
    `couldnt find a char at ${parserState.index}`
  );
});

const between = (left: string, right: string) => (parser: Parser) => {
  return sequenceOf([str(left), parser, str(right)]).map(([l, m, r]) => {
    return m;
  });
};

const betweenBracket = between("(", ")");

const numberParser = digits.map((out: any) => {
  return {
    type: "number",
    value: Number(out),
  };
});

const letterParser = letters.map((out: any) => {
  return {
    type: "string",
    value: out,
  };
});

const diceParser = sequenceOf([digits, str("d"), digits]).map(([n, _, s]) => {
  return {
    type: "diceroll",
    value: [Number(n), Number(s)],
  };
});

const expP = sequenceOf([letterParser, str(":")])
  .map((res) => res[0])
  .chain((sample: ParserState) => {
    if (!sample.error) {
      return diceParser;
    }
  });

// TODO:Implement a parser for a BIT
// Implement parsr for one byte and zero byte
// complete the parsing challenge

const trueP = str("true");
const falseP = str("false");

const op = choice([str("+"), str("-"), str("/"), str("*")]).map((res) => {
  return {
    type: "op",
    value: res,
  };
});

// const lispyThunk = (f : Function) => {
//   let parser = f()
//   return parser
// }

const spaceP = str(" ");

const lazy = (parserThunk) =>
  new Parser((parserState) => {
    let p: Parser = parserThunk();
    return p.parserStateTransformerFn(parserState);
  });

const gg = choice([lazy(() => lispy), numberParser]);

const lispy = betweenBracket(
  sequenceOf([
    letterParser.map((res) => res.value),
    spaceP,
    op.map((res) => res.value),
    spaceP,
    gg,
    spaceP,
    gg,
  ])
).map(([id, spc1, op, spc2, expr1, spc3, expr2 ]) => {
  // console.log((d))
  return {
    type : "expr",
    id : id,
    op : op,
    A : expr1,
    B : expr2
  }
});

//basic tree walker just to test

let div = (a, b) => a / b;
let mul = (a, b) => a * b;
let add = (a, b) => a + b;
let sub = (a, b) => a - b;

let fnMap = {
  "+": add,
  "-": sub,
  "*": mul,
  "/": div,
};

const evl = (tree: any) => {
  if (tree.type === "number") {
    return tree.value;
  }
  if (tree.type === "expr") {
    let fn = fnMap[tree.op];
    return fn(evl(tree.A), evl(tree.B));
  }
  return "issue";
};

// (+ 4 5 (- 9 0))
// (+ 3 (+)d
let v = lispy.run("(left * (right * 2 (right * 1 1)) 1)") as ParserState;

console.log(evl(v.result));
//works great now we an move on to the next phase
