interface ParserState {
  index: number;
  targetString: string;
  result: any
  error: string | null 
}

export type Result = Array<any>

export type ParserFn = (p0 : ParserState) => (ParserState | Function)